class signInPage {
        get emailInput()            { return $("[type=email]")}
        get nextButton()            { return $(".VfPpkd-LgbsSe.VfPpkd-LgbsSe-OWXEXe-k8QpJ")}
        get repeatButton()          { return $(".WpHeLc.VfPpkd-mRLv6")}
        get supportButton()         { return $("a[href=\"https://support.google.com/accounts/answer/7675428?hl=ru\"]")}



    async inputEmail(login) {
        await console.log('inputEmail');
        await (await this.emailInput).setValue(login);
        await console.log('click on the Next button');
        await (await this.nextButton).waitForClickable();
        await (await this.nextButton).click();
    }

    async clickRepeatButton() {
        await console.log('click on the Repeat button');
        await (await this.repeatButton).waitForClickable();
        await (await this.repeatButton).click();
    }

    async clickSupportButton() {
        await console.log('click on the Support button');
        await (await this.supportButton).waitForClickable();
        await (await this.supportButton).click();
    }

}
module.exports = new signInPage();