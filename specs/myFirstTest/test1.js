const signInPage = require('../../pageobjects/signInPage');
const supportPage = require('../../pageobjects/supportPage');

describe('My Google Authorisation Test', () => {

        it('Authorization 1', async () => {
            browser.url('spreadsheets/d/1wV_b5TnfUw-9s-KzqhTHluDfVMkb2lQTLNUfty0ufnw/edit#gid=0');
            await signInPage.inputEmail('nastenakiss@gmail.com');
            await signInPage.clickRepeatButton();
            await signInPage.inputEmail('nastenakiss@gmail.com');
            await signInPage.clickSupportButton();
            browser.switchWindow('https://support.google.com/accounts/answer/7675428?hl=ru');
            await expect(supportPage.H1).toHaveTextContaining("Как войти в аккаунт в поддерживаемом браузере");
        });
});